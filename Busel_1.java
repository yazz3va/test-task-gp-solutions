import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

class Busel_1 {



    public static void main(String[] args) throws  IOException{

        try(Scanner in = new Scanner(new FileInputStream("input.txt"));
                                  PrintWriter writer = new PrintWriter("output.txt")) {

            int size = in.nextInt();

            int[] array = new int[size];

            for (int i = 0; i < size; i++)
                array[i] = in.nextInt();


            int min, min_hight, max, max_low;

            min = min_hight = Integer.MAX_VALUE;
            max = max_low = Integer.MIN_VALUE;


            for (int x : array) {
                if (x <= min) {
                    if (min != Integer.MAX_VALUE) min_hight = min;
                    min = x;
                } else if (x < min_hight && x > min) min_hight = x;


                if (x >= max) {
                    if (max != Integer.MIN_VALUE) max_low = max;
                    max = x;
                } else if (x > max_low && x < max) max_low = x;

            }


            writer.write(min + " " + max + " " + min_hight + " " + max_low);
        }
    }
}
