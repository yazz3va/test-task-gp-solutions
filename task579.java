import java.util.*;
import java.io.*;

public class Main{
    public static void main(String[] argv) throws IOException{
        new Main().run();
    }
    PrintWriter pw;
    Scanner scanner;
    int[] arr;
    public void run() throws IOException{
        scanner = new Scanner(new File("input.txt"));
        arr = new int[scanner.nextInt()];

        for(int i=0; i< arr.length; i++){
            arr[i] = scanner.nextInt();
        }

        arr = find(arr);

        pw = new PrintWriter(new File("output.txt"));


        pw.write( String.valueOf(arr.length) + System.lineSeparator());
        for(int x: arr){
            pw.write(String.valueOf(x) + ' ');
        }
        pw.close();
    }

    private  int[] find(int[] source_arr){
        int[] minus_arr = new int[source_arr.length];
        int[] plus_arr = new int[source_arr.length];
        int minus_sum = 0, plus_sum =0;
        int minus_count =0, plus_count =0;

        for(int i=0; i<source_arr.length; i++){
            if(source_arr[i] >= 0){
                plus_arr[plus_count++] = i+1;
                plus_sum+=source_arr[i];
            }else {
                minus_arr[minus_count++] = i+1;
                minus_sum += source_arr[i];
            }
        }

        if(plus_sum > Math.abs(minus_sum)){
            int[] ret = new int[plus_count];
            System.arraycopy(plus_arr, 0, ret, 0, plus_count);
            return  ret;
        }else {
            int[] ret = new int[minus_count];
            System.arraycopy(minus_arr, 0, ret, 0, minus_count);
            return  ret;
        }
    }
}