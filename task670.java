import java.util.*;
import java.io.*;

public class Main{
    public static void main(String[] argv) throws IOException{
        new Main().run();
    }
    PrintWriter pw;
    Scanner sc;
    public void run() throws IOException{
        sc = new Scanner(new File("input.txt"));
        int in_number =sc.nextInt();
        int out_number = find(in_number);
        pw = new PrintWriter(new File("output.txt"));
        pw.print(out_number);
        pw.close();
    }

    private int find(int src_number){
        int result = 0;
        for(int i = 0; i<src_number; i++){
            do{
                result++;
            }while(sameDigits(result));
        }
        return result;
    }

    private boolean sameDigits(int num){
        char[] numbers = String.valueOf(num).toCharArray();

        for(int i=0; i<numbers.length; i++){
            for (int j=0; j<numbers.length; j++){
                if(i==j)continue;
                if(numbers[i] == numbers[j])return true;
            }
        }
        return  false;
    }
}