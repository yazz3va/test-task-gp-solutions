import java.io.*;

class Main{
    public static void main(String[] argv) throws IOException{
        new Main().run();
    }
    PrintWriter pw;
    byte[]  file_buffer;
    FileInputStream file;

    int[][][] matrix_src;
    int target_i, target_j;
    int modul = 0;
    int size_n = 0;

    public void run() throws IOException{

        file = new FileInputStream(new File("input.txt"));
        file_buffer = new byte[500_000];
        file.read(file_buffer);
        int count_m = nextInt();
        size_n = nextInt();
        target_i = nextInt()-1;
        target_j = nextInt()-1;

        modul = nextInt();

        matrix_src = filling(count_m,size_n);


        int[] matrix_res = matrix_src[0][target_i];


        for(int i=1; i<matrix_src.length; i++){
            matrix_res = matrixMultiplection(matrix_res, matrix_src[i]);
        }

        pw = new PrintWriter(new File("output.txt"));
        pw.print(matrix_res[target_j]);
        file.close();
        pw.close();
    }

    private int nextInt() throws IOException {
        char ch;
        do{
            ch = getChar();
        }while (!Character.isDigit(ch));

        int result = Character.getNumericValue(ch);

        for (char i= getChar(); Character.isDigit(i); i=getChar()){
            result = (result*10)+Character.getNumericValue(i);
        }
        return result;
    }

    int position_file = 0;
    private char getChar()throws IOException{
        if(position_file == file_buffer.length) {position_file = 0;  file.read(file_buffer);}
        return (char)file_buffer[position_file++];
    }

    private int[] matrixMultiplection(int[] matrix_a, int[][] matrix_b){
        int[] res = new int[size_n];

        for (int j = 0; j <size_n; j++) {
            for (int k = 0; k < size_n; k++) {
                res[j] += matrix_a[k] * matrix_b[k][j];
            }
            res[j] %= modul;
        }

        return  res;
    }
    private int[][][] filling(int size_m,int size_n)throws IOException {
        int[][][] matrix = new int[size_m][size_n][size_n];

        for (int m=0; m<size_m; m++) {
            for (int i = 0; i < size_n; i++) {
                for (int j = 0; j < size_n; j++) {
                    matrix[m][i][j] = nextInt();
                }
            }
        }
        return matrix;
    }
}