import java.util.*;
import java.io.*;

public class Main{
    public static void main(String[] argv) throws IOException{
        new Main().run();
    }
    PrintWriter pw;
    BufferedReader sc;
    public void run() throws IOException{
        sc = new BufferedReader (new FileReader("input.txt"));
        String one_line = sc.readLine();
        String two_line = sc.readLine();
        pw = new PrintWriter(new File("output.txt"));
        pw.print(match(one_line, two_line));
        pw.close();
    }

    private String match(String str1, String str2){
        char[] string1 = str1.toCharArray();
        char[] string2 = str2.toCharArray();

        int last_key = 0;

        mark: for(int i = 0; i<string1.length; i++){

            while(last_key<string2.length){
                if(string1[i] == string2[last_key++]) continue mark;
            }
            return  "NO";
        }
        return "YES";
    }
}