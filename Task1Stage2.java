import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Main {

    public static void  main(String[] args) {

        ArrayList<String> input = new ArrayList<>();
        ArrayList<String> output = new ArrayList<>();

        System.out.print("Arguments: ");
        for(String str: args) System.out.print(str+ " | ");
        System.out.println();


        System.out.println("Enter the lines:");
        try(BufferedReader buffer=new BufferedReader(new InputStreamReader(System.in))){

            for (String str = buffer.readLine(); str.length()>0; str = buffer.readLine()){
                input.add(str);
            }

        }catch (IOException e){
           System.err.println("Ошибка ввода");
        }

       mark: for(String str_line: input){
            String[] str = str_line.split("[ ;]");
            for(int i=0; i<str.length; i++){
               for (String regex : args) {
                   if (str[i].equals(regex) || str[i].matches(regex)) {
                       output.add(str_line);
                       continue mark;
                   }
               }
           }
        }

        System.out.println("Output:");
        for (String str: output) System.out.println(str);

    }

}