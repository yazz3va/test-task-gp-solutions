
import java.math.BigDecimal;

public class Token {

    public char kind;
    private  String value;

    public  Token(String value){
         if(Character.isDigit(value.charAt(0))) kind = '0';
         else kind = value.charAt(0);
        this.value = value;
    }

    public BigDecimal decimalvalue(){
         return new BigDecimal(value);
    }
}
