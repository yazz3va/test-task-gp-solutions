
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args){
          CalculatorDecimal calculator = new CalculatorDecimal();

          String last_line = new String();


          System.out.println("Для выхода введите exit");
              try (BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in))) {

                  while(!last_line.equals("exit")) {

                      System.out.println("Введите выражение:");
                      last_line = buffer.readLine();


                      if (!last_line.equals("exit")) {
                          System.out.print("Ответ: ");
                          try {
                              System.out.println(calculator.expression(last_line));
                          } catch (ArithmeticException e) {
                              System.out.println(e);
                          }

                      }
                  }
              }
              catch (IOException e) {
                  System.err.println("Ошибка ввода");
              }


    }
}
