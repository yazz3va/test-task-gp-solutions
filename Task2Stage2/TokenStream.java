



public class TokenStream {

    private String exp;
    private int exp_pos;
    private Token token;

    public TokenStream(String exp){
        this.exp = exp.replace(',', '.');
        this.exp_pos = 0;
        this.token = null;
    }

    public Token get(){
        if(token != null) {Token t = token; token = null; return t;}
        String value = new String();
        boolean error = false;
        while (exp_pos< exp.length()){
            if(Character.isDigit(exp.charAt(exp_pos)) || exp.charAt(exp_pos) == '.'){
                if(error){
                    if(value.length() == 0) error = false;
                        else throw new ArithmeticException("Невалидные данные");
                }
                value+=exp.charAt(exp_pos++);
                continue;
            }
            if(Character.isWhitespace(exp.charAt(exp_pos))){
                exp_pos++;
                error = true;
                continue;
            }
            if(value.length()>0){
                break;
            }
            value+=exp.charAt(exp_pos++);
            break;
        }
        if(value.length() == 0) value+=';';
        return  new Token(value);
    }

    public void put(Token t){
        token = t;
    }
}
