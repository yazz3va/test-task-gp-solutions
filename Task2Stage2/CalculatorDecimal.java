import java.math.BigDecimal;

public class CalculatorDecimal {

     TokenStream tokenStream;

    CalculatorDecimal(){

    }



    public BigDecimal expression(String exp)  {

        tokenStream = new TokenStream(exp);

        return lowPriority();
    }

    private BigDecimal lowPriority() {
        BigDecimal left = mediumPriority();
        Token t = tokenStream.get();
        while (true) {
            switch (t.kind) {
                case '+':
                     left = left.add(mediumPriority());
                     t = tokenStream.get();
                     break;
                case '-':
                     left = left.subtract(mediumPriority());
                    t = tokenStream.get();
                    break;
                default:

                    if(!(t.kind == ';' || t.kind == ')')) throw new ArithmeticException("Невалидные данные");

                    tokenStream.put(t);
                    return left;
            }
        }


    }

    private BigDecimal mediumPriority(){
        BigDecimal left = highPriority();
        Token t = tokenStream.get();
        while (true){
            switch (t.kind){
                case '*':
                    left = left.multiply(highPriority());
                    t = tokenStream.get();
                    break;
                case '/':
                    BigDecimal bd = highPriority();

                    if(bd.compareTo(BigDecimal.ZERO) == 0){
                      throw new ArithmeticException("Деление на 0");
                    }
                    left = left.divide(bd);
                    t = tokenStream.get();
                    break;
                    default:
                        tokenStream.put(t);
                        return left;
            }
        }
    }

    private BigDecimal highPriority(){
        BigDecimal left = primary();
        Token t = tokenStream.get();
        while (true){
            switch (t.kind){
                case '^':
                    left = new BigDecimal(Math.pow(left.doubleValue(), primary().doubleValue()));
                    t = tokenStream.get();
                    break;
                default:
                    tokenStream.put(t);
                    return left;
            }
        }
    }

    private BigDecimal primary() {
        Token t = tokenStream.get();
        switch (t.kind){
            case '(':
            {
                BigDecimal bd = lowPriority();
                t= tokenStream.get();
                if(t.kind != ')'){
                    throw new ArithmeticException("Невалидные данные");
                }
                return  bd;
            }
            case '-':
                t = tokenStream.get();
                return  t.decimalvalue().negate();
            case '+':
                t = tokenStream.get();
            case '0':
                return  t.decimalvalue();
                default:
                  throw new ArithmeticException("Невалидные данные");
        }

    }
}
